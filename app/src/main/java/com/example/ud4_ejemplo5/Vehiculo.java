package com.example.ud4_ejemplo5;

public class Vehiculo {
    private String nombre; // Nombre del vehículo.
    private String aparicion; // Serie o película donde aparece.
    private String imagen; // Nombre de la imagen del vehículo.

    public Vehiculo(String nombre, String aparicion, String imagen) {
        this.nombre = nombre;
        this.aparicion = aparicion;
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public String getAparicion() {
        return aparicion;
    }

    public String getImagen() {
        return imagen;
    }
}