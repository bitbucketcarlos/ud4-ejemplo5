package com.example.ud4_ejemplo5;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class VehiculoAdapter extends RecyclerView.Adapter<VehiculoAdapter.TarjetaViewHolder>{

    public ArrayList<Vehiculo> lista;
    private View.OnClickListener onClickListener; // Atributo para el evento

    class TarjetaViewHolder extends RecyclerView.ViewHolder {

        public ImageView imagen;
        public TextView texto;

        public TarjetaViewHolder(View itemView) {
            super(itemView);

            imagen = itemView.findViewById(R.id.imagenVehiculo);
            texto = itemView.findViewById(R.id.textoVehiculo);
        }
    }

    public VehiculoAdapter(ArrayList<Vehiculo> lista) {
        this.lista = lista;
    }

    @NonNull
    @Override
    public TarjetaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());

        View view=inflater.inflate(R.layout.elementos_tarjeta,parent,false);

        view.setOnClickListener(this.onClickListener);

        TarjetaViewHolder miViewHolder = new TarjetaViewHolder(view);

        return miViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TarjetaViewHolder holder, int position) {
        Vehiculo vehiculo = lista.get(position);

        // Obtenemos la dirección Uri de la imagen en nuestro proyecto
        Uri uriImagen = Uri.parse("android.resource://" + Objects.requireNonNull(MainActivity.class.getPackage()).getName() + "/drawable/" + vehiculo.getImagen());

        holder.imagen.setImageURI(uriImagen);
        holder.texto.setText(vehiculo.getNombre());

    }

    @Override
    public int getItemCount() {
        if(lista == null)
            return 0;
        else
            return lista.size();
    }
    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}

