package com.example.ud4_ejemplo5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Vehiculo> vehiculos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Creamos el array de vehiculos.
        vehiculos.add(new Vehiculo("Ecto1", "Los cazafantasmas", "ecto1"));
        vehiculos.add(new Vehiculo("DeLorean", "Regreso al futuro", "delorian"));
        vehiculos.add(new Vehiculo("Kitt", "El coche fantástico", "kitt"));
        vehiculos.add(new Vehiculo("Halcón Milenario", "Star Wars", "halcon"));
        vehiculos.add(new Vehiculo("TARDIS", "Doctor Who", "tardis"));

        RecyclerView recycler = findViewById(R.id.recyclerView);

        recycler.setHasFixedSize(true);

        // Creamos el Grid de 2 columnas
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(this, 2);
        recycler.setLayoutManager(mGridLayoutManager);

        VehiculoAdapter adapter = new VehiculoAdapter(vehiculos);

        // Asignamos el evento creado en VehiculoAdapter
        adapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vehiculo vehiculo = vehiculos.get(recycler.getChildAdapterPosition(v));
                Toast.makeText(MainActivity.this, vehiculo.getAparicion(), Toast.LENGTH_SHORT).show();
            }
        });

        recycler.setAdapter(adapter);

    }
}